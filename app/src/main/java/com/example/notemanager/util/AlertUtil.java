package com.example.notemanager.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AlertUtil {

    public static void showRemoveNoteAlert(Context context, DialogInterface.OnClickListener listener) {
        createAlert(context, "Usuń", "Czy na pewno usunąc notatkę?", listener).show();
    }

    public static void showSaveNoteAlert(Context context, DialogInterface.OnClickListener listener) {
        createAlert(context, "Zapisz", "Czy na pewno chcesz zapisać i wyjść?", listener).show();
    }

    public static void showExitAlert(Context context, DialogInterface.OnClickListener listener) {
        createAlert(context, "Wyjdź", "Czy na pewno chcesz wyjść?", listener).show();
    }

    private static AlertDialog.Builder createAlert(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setPositiveButton("Tak", listener);
        alert.setNegativeButton("Nie", (dialog, which) -> dialog.dismiss());
        return alert;
    }
}
