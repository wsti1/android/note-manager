package com.example.notemanager.util;

import java.time.LocalDate;
import java.time.LocalTime;

public class DateUtil {

    public static LocalDate parseDate(String date) {
        if (date != null && !date.isEmpty()) {
            return LocalDate.parse(date);
        }
        return null;
    }

    public static LocalTime parseTime(String time) {
        if (time != null && !time.isEmpty()) {
            return LocalTime.parse(time);
        }
        return null;
    }
}
