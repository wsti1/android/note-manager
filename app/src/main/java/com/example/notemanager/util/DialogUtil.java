package com.example.notemanager.util;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.EditText;

import java.time.LocalDate;
import java.time.LocalTime;

public class DialogUtil {

    @SuppressLint("DefaultLocale")
    public static void showDatePickerDialog(Context context, LocalDate initialDate, EditText dateText) {
        new DatePickerDialog(context, (view, year, month, day) -> {
            dateText.setText(LocalDate.of(year, month, day).toString());
        }, initialDate.getYear(), initialDate.getMonthValue(), initialDate.getDayOfMonth()).show();
    }

    public static void showTimePickerDialog(Context context, LocalTime localTime, EditText timeText) {
        new TimePickerDialog(context, (view, hour, minute) -> {
            timeText.setText(LocalTime.of(hour, minute).toString());
        }, localTime.getHour(), localTime.getMinute(), true).show();
    }
}
