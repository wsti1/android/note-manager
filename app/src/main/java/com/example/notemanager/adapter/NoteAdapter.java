package com.example.notemanager.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.notemanager.R;
import com.example.notemanager.domain.Note;
import com.example.notemanager.repository.NoteRepository;

import java.util.Objects;

public class NoteAdapter extends ArrayAdapter<Note> {

    private Context context;
    private NoteRepository noteRepository;

    public NoteAdapter(Context context, NoteRepository noteRepository) {
        super(context, 0, noteRepository.findAll());
        this.context = context;
        this.noteRepository = noteRepository;
    }

    public void insert(Note note) {
        noteRepository.insert(note);
        notifyDataSetChanged();
    }

    public void update(String id, Note holder) {
        noteRepository.update(id, holder);
        notifyDataSetChanged();
    }

    public void delete(Note note) {
        noteRepository.delete(note);
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        noteRepository.writeNotes(context.getFilesDir());
        super.notifyDataSetChanged();
    }

    public Note getNoteAt(int position) {
        return noteRepository.findAt(position);
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        Note note = Objects.requireNonNull(getItem(position));

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.note_row_item, parent, false);
        }
        TextView title = view.findViewById(R.id.note_title);
        title.setText(note.getTitle());

        TextView content = view.findViewById(R.id.note_content);
        content.setText(note.getContent());

        if (note.getCategory() != null) {
            LinearLayout layout = view.findViewById(R.id.layout);
            layout.setBackgroundColor(Color.parseColor(note.getCategory().getColor()));
        }
        return view;
    }
}
