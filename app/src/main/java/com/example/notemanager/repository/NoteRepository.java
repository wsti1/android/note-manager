package com.example.notemanager.repository;

import com.example.notemanager.domain.Note;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class NoteRepository {

    private List<Note> notes;

    public NoteRepository(File file) {
        this.notes = readNotes(file);
    }

    public void insert(Note note) {
        notes.add(note);
    }

    public void update(String id, Note holder) {
        for (Note note : notes) {
            if (note.getId().equals(id)) {
                note.copy(holder);
            }
        }
    }

    public void delete(Note task) {
        notes.remove(task);
    }

    public List<Note> findAll() {
        return notes;
    }

    public Note findAt(int index) {
        return notes.get(index);
    }

    public void writeNotes(File file){
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file.getPath() + "notes"))) {
            outputStream.writeObject(notes);
        }
        catch (IOException e) {
            throw new IllegalStateException("Failed to write notes");
        }
    }

    @SuppressWarnings("unchecked")
    private List<Note> readNotes(File file) {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file.getPath() + "notes"))) {
            return (ArrayList<Note>) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            return new ArrayList<>();
        }
    }
}
