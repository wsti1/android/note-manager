package com.example.notemanager.domain;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.widget.RadioButton;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.Consumer;

public enum Category implements Serializable {

    OTHER(1, "Inne", "#7C1966DA"),
    WORK(2, "Praca", "#7C19DA43"),
    HOME(3, "Dom", "#7CDAD719"),
    IMPORTANT(4, "Ważne", "#7CDA1919");

    private int id;
    private String text;
    private String color;

    Category(int id, String text, String color) {
        this.id = id;
        this.text = text;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getColor() {
        return color;
    }

    @SuppressLint("SetTextI18n")
    public RadioButton toRadioButton(Context context) {
        RadioButton radioButton = new RadioButton(context);
        radioButton.setId(id);
        radioButton.setText(text + workaroundForFixedSize());
        radioButton.setBackgroundColor(Color.parseColor(color));
        return radioButton;
    }

    private String workaroundForFixedSize() {
        return new String(new char[100]).replace('\0', ' ').substring(text.length());
    }

    public static Category valueOfText(String text) {
        for (Category category : values()) {
            if (category.getText().equals(text)) {
                return category;
            }
        }
        return null;
    }

    public static void forEach(Consumer<Category> action) {
        Arrays.asList(values()).forEach(action);
    }
}
