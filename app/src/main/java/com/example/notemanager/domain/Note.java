package com.example.notemanager.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

public class Note  implements Serializable {

    private String id;
    private String title;
    private String content;
    private Category category;
    private LocalDate finishDate;
    private LocalTime finishTime;

    public Note(String title, String content, Category category, LocalDate finishDate, LocalTime finishTime) {
        this.id = UUID.randomUUID().toString();
        this.title = title;
        this.content = content;
        this.category = category;
        this.finishDate = finishDate;
        this.finishTime = finishTime;
    }

    public void copy(Note note) {
        this.title = note.title;
        this.content = note.content;
        this.category = note.category;
        this.finishDate = note.finishDate;
        this.finishTime = note.finishTime;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    public LocalDate getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }

    public LocalTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalTime finishTime) {
        this.finishTime = finishTime;
    }
}
