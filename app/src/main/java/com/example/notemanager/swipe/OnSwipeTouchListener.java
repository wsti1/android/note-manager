package com.example.notemanager.swipe;

public interface OnSwipeTouchListener {

    void onSwipeRight(int position);

    void onSwipeLeft(int position);
}
