package com.example.notemanager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notemanager.adapter.NoteAdapter;
import com.example.notemanager.domain.Category;
import com.example.notemanager.domain.Note;
import com.example.notemanager.repository.NoteRepository;
import com.example.notemanager.swipe.OnSwipeTouch;
import com.example.notemanager.swipe.OnSwipeTouchListener;
import com.example.notemanager.util.AlertUtil;
import com.example.notemanager.util.DateUtil;
import com.example.notemanager.util.ToastUtil;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Optional;

import static com.example.notemanager.AddEditNoteActivity.EXTRA_CATEGORY;
import static com.example.notemanager.AddEditNoteActivity.EXTRA_CONTENT;
import static com.example.notemanager.AddEditNoteActivity.EXTRA_FINISH_DATE;
import static com.example.notemanager.AddEditNoteActivity.EXTRA_FINISH_TIME;
import static com.example.notemanager.AddEditNoteActivity.EXTRA_ID;
import static com.example.notemanager.AddEditNoteActivity.EXTRA_TITLE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, OnSwipeTouchListener {

    private Note selectedNote;
    private NoteAdapter noteAdapter;

    @Override
    @SuppressLint("ClickableViewAccessibility")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(this);

        ListView noteList = findViewById(R.id.ListView);
        noteList.setOnItemClickListener(this);

        noteAdapter = new NoteAdapter(this, new NoteRepository(this.getFilesDir()));
        noteList.setAdapter(noteAdapter);
        noteList.setOnTouchListener(new OnSwipeTouch(this, noteList, this));
    }

    @Override
    public void onSwipeRight(int position) {
        onSwipeDeleteNote(position);
    }

    @Override
    public void onSwipeLeft(int position) {
        onSwipeDeleteNote(position);
    }

    private void onSwipeDeleteNote(int position) {
        AlertUtil.showRemoveNoteAlert(this, (dialog, which) -> {
            noteAdapter.delete(noteAdapter.getNoteAt(position));
            ToastUtil.showToast(this, "Notatka została usunięta");
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_button) {
            selectedNote = null;
            startActivityForResult(createArrivalIntent(), 0);
        }
    }

    private Intent createArrivalIntent() {
        Optional<Note> note = Optional.ofNullable(selectedNote);
        Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
        intent.putExtra(EXTRA_ID, note.map(Note::getId).orElse(""));
        intent.putExtra(EXTRA_TITLE, note.map(Note::getTitle).orElse(""));
        intent.putExtra(EXTRA_CONTENT, note.map(Note::getContent).orElse(""));
        intent.putExtra(EXTRA_CATEGORY, note.map(Note::getCategory).map(Enum::name).orElse(Category.OTHER.name()));
        intent.putExtra(EXTRA_FINISH_DATE, note.map(Note::getFinishDate).map(LocalDate::toString).orElse(""));
        intent.putExtra(EXTRA_FINISH_TIME, note.map(Note::getFinishTime).map(LocalTime::toString).orElse(""));
        return intent;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Note note = (Note) parent.getItemAtPosition(position);
        if (position != RecyclerView.NO_POSITION) {
            selectedNote = note;
            startActivityForResult(createArrivalIntent(), 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK) {
            Objects.requireNonNull(intent);

            String id = intent.getStringExtra(EXTRA_ID);
            String title = intent.getStringExtra(EXTRA_TITLE);
            String content = intent.getStringExtra(EXTRA_CONTENT);
            Category category = Category.valueOfText(intent.getStringExtra(EXTRA_CATEGORY));
            LocalDate finishDate = DateUtil.parseDate(intent.getStringExtra(EXTRA_FINISH_DATE));
            LocalTime finishTime = DateUtil.parseTime(intent.getStringExtra(EXTRA_FINISH_TIME));

            if (id == null || id.isEmpty()) {
                Note note = new Note(title, content, category, finishDate, finishTime);
                noteAdapter.insert(note);
                ToastUtil.showToast(this, "Notatka została dodana");
            } else {
                Note holder = new Note(title, content, category, finishDate, finishTime);
                noteAdapter.update(id, holder);
                ToastUtil.showToast(this, "Notatka została zaktualizowana");
            }
        } else {
            ToastUtil.showToast(this, "Notatka nie została zapisana");
        }
    }
}

