package com.example.notemanager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.example.notemanager.domain.Category;
import com.example.notemanager.notifier.NoteNotifier;
import com.example.notemanager.util.AlertUtil;
import com.example.notemanager.util.DialogUtil;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class AddEditNoteActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_ID = "EXTRA_ID";
    public static final String EXTRA_TITLE = "EXTRA_TITLE";
    public static final String EXTRA_CONTENT = "EXTRA_CONTENT";
    public static final String EXTRA_CATEGORY = "EXTRA_CATEGORY";
    public static final String EXTRA_FINISH_DATE = "EXTRA_FINISH_DATE";
    public static final String EXTRA_FINISH_TIME = "EXTRA_FINISH_TIME";

    private String noteId;
    private EditText titleText;
    private EditText contentText;
    private EditText dateText;
    private EditText timeText;
    private RadioGroup categoryGroup;

    private EditText phoneText;
    private Button smsButton;
    private EditText emailText;
    private Button emailButton;

    private NoteNotifier noteNotifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_note);

        titleText = findViewById(R.id.title_text);
        contentText = findViewById(R.id.content_text);
        dateText = findViewById(R.id.date_text);
        timeText = findViewById(R.id.time_text);
        phoneText = findViewById(R.id.phone_text);
        smsButton = findViewById(R.id.sms_button);
        emailText = findViewById(R.id.email_text);
        emailButton = findViewById(R.id.email_button);

        categoryGroup = findViewById(R.id.category);
        Category.forEach(category -> categoryGroup.addView(category.toRadioButton(this)));

        Button dateButton = findViewById(R.id.date_button);
        dateButton.setOnClickListener(this);

        Button timeButton = findViewById(R.id.time_button);
        timeButton.setOnClickListener(this);

        Button clearDateTimeButton = findViewById(R.id.clear_datetime);
        clearDateTimeButton.setOnClickListener(this);

        Button saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);

        Button exitButton = findViewById(R.id.exit_button);
        exitButton.setOnClickListener(this);

        smsButton.setOnClickListener(this);
        emailButton.setOnClickListener(this);

        noteNotifier = new NoteNotifier(this);

        extractReceivedIntent();
    }

    private void extractReceivedIntent() {
        Intent intent = Objects.requireNonNull(getIntent());
        noteId = intent.getStringExtra(EXTRA_ID);
        categoryGroup.check(Category.valueOf(intent.getStringExtra(EXTRA_CATEGORY)).getId());
        if (noteId != null && !noteId.isEmpty()) {
            titleText.setText(intent.getStringExtra(EXTRA_TITLE));
            contentText.setText(intent.getStringExtra(EXTRA_CONTENT));
            dateText.setText(intent.getStringExtra(EXTRA_FINISH_DATE));
            timeText.setText(intent.getStringExtra(EXTRA_FINISH_TIME));
        } else {
            LinearLayout smsLayout = findViewById(R.id.sms_layout);
            smsLayout.setVisibility(View.INVISIBLE);

            LinearLayout emailLayout = findViewById(R.id.email_layout);
            emailLayout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.date_button:
                DialogUtil.showDatePickerDialog(this, LocalDate.now(), dateText);
                break;
            case R.id.time_button:
                DialogUtil.showTimePickerDialog(this, LocalTime.now(), timeText);
                break;
            case R.id.clear_datetime:
                dateText.setText("");
                timeText.setText("");
                break;
            case R.id.save_button:
                AlertUtil.showSaveNoteAlert(this, (dialog, which) -> {
                    setResult(RESULT_OK, createReturnIntent());
                    finish();
                });
                break;
            case R.id.exit_button:
                AlertUtil.showExitAlert(this, (dialog, which) -> finish());
                break;
            case R.id.sms_button:
                noteNotifier.sendSms(phoneText.getText(), contentText.getText());
                break;
            case R.id.email_button:
                noteNotifier.sendEmail(emailText.getText(), titleText.getText(), contentText.getText());
        }
    }

    private Intent createReturnIntent() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_ID, noteId);
        intent.putExtra(EXTRA_TITLE, String.valueOf(titleText.getText()));
        intent.putExtra(EXTRA_CONTENT, String.valueOf(contentText.getText()));
        intent.putExtra(EXTRA_CATEGORY, String.valueOf(getSelectedCategory().getText()).trim());
        intent.putExtra(EXTRA_FINISH_DATE, String.valueOf(dateText.getText()));
        intent.putExtra(EXTRA_FINISH_TIME, String.valueOf(timeText.getText()));
        return intent;
    }

    private RadioButton getSelectedCategory() {
        return findViewById((categoryGroup).getCheckedRadioButtonId());
    }
}
