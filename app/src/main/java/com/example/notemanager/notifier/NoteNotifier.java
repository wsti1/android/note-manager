package com.example.notemanager.notifier;

import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.text.Editable;

import com.example.notemanager.util.ToastUtil;

public class NoteNotifier {

    private Context context;

    public NoteNotifier(Context context) {
        this.context = context;
    }

    public void sendSms(Editable phone, Editable content) {
        String phoneNumber = String.valueOf(phone);
        String smsContent = String.valueOf(content);

        if (phoneNumber.length() != 9) {
            ToastUtil.showToast(context, "Proszę podać poprawny numer telefonu");
            return;
        }
        try{
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber, null, smsContent, null, null);
            ToastUtil.showToast(context, "Wysłano SMS");
        } catch (SecurityException e) {
            ToastUtil.showToast(context, "Brak uprawnień do wysyłania SMS");
        } catch (Exception e){
            ToastUtil.showToast(context, "Wysyłanie SMS nie powiodło się");
        }
    }

    public void sendEmail(Editable email, Editable title, Editable content) {
        String emailAddress = String.valueOf(email);
        String emailSubject = String.valueOf(title);
        String emailText = String.valueOf(content);

        if (emailAddress.isEmpty()) {
            ToastUtil.showToast(context, "Proszę podać poprawny adres email");
            return;
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
        intent.putExtra(Intent.EXTRA_SUBJECT, emailSubject);
        intent.putExtra(Intent.EXTRA_TEXT, emailText);
        try {
            context.startActivity(Intent.createChooser(intent, "Wysyłanie emaila..."));
        } catch (android.content.ActivityNotFoundException ex) {
            ToastUtil.showToast(context, "Brak klienta skrzynki email");
        }
    }
}
